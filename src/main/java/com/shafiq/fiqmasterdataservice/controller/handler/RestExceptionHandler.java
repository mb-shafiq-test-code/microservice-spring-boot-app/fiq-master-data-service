package com.shafiq.fiqmasterdataservice.controller.handler;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException e, HttpHeaders headers, HttpStatus httpStatus, WebRequest request) {
        return new ResponseEntity<>(null,null, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {IllegalArgumentException.class, IllegalStateException.class})
    protected ResponseEntity<Object> handleConflict(RuntimeException e, WebRequest request) {
        return new ResponseEntity<>(null,null, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value={ConversionFailedException.class})
    public ResponseEntity<Object> handleConversionConflict(RuntimeException e, WebRequest request) {
        return new ResponseEntity<>(null,null, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleSQLConflict(RuntimeException e, WebRequest request) {
        return new ResponseEntity<>(null,null, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleGlobalConflict(Exception e) {
        return new ResponseEntity<>(null,null, HttpStatus.BAD_REQUEST);
    }


}
