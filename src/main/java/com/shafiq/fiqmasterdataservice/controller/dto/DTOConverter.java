package com.shafiq.fiqmasterdataservice.controller.dto;

import com.shafiq.fiqmasterdataservice.entities.Account;

import java.util.Optional;

public class DTOConverter {
    public static AccountDTO convertAccount(Account account) {
        return Optional.ofNullable(account).map(p -> AccountDTO.builder()
                .active(p.getActive())
                .id(p.getId())
                .fullName(p.getFullName())
                .email(p.getEmail())
                .mobileNo(p.getMobileNo())
                .birthDate(p.getBirthDate())
                .task(p.getTask())
                .createdDate(p.getCreatedDate())
                .lastUpdateDate(p.getLastUpdateDate())
                .deleted(p.getDeleted())
                .build()).orElse(null);
    }
}
