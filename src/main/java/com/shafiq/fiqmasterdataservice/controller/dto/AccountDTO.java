package com.shafiq.fiqmasterdataservice.controller.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class AccountDTO {
    private String id;
    private Date createdDate;
    private Boolean active;
    private Boolean deleted;
    private String fullName;
    private String email;
    private String mobileNo;
    private Date birthDate;
    private String task;
    private Date lastUpdateDate;
}
