package com.shafiq.fiqmasterdataservice.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.shafiq.fiqmasterdataservice.services.FieldAttributes;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class PageAccountRequest {

    @JsonProperty("task")
    private String task;

    @FieldAttributes(nullable = false, minValue = 0)
    @JsonProperty("page")
    private Integer page;

    @FieldAttributes(nullable = false, minValue = 1)
    @JsonProperty("size")
    private Integer size;
}
