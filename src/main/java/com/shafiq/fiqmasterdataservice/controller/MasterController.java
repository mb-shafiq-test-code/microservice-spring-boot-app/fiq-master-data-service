package com.shafiq.fiqmasterdataservice.controller;

import com.shafiq.fiqmasterdataservice.controller.dto.AccountDTO;
import com.shafiq.fiqmasterdataservice.controller.dto.DTOConverter;
import com.shafiq.fiqmasterdataservice.controller.dto.PageAccountRequest;
import com.shafiq.fiqmasterdataservice.services.MasterService;
import com.shafiq.fiqmasterdataservice.utils.BaseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@Slf4j
public class MasterController {

    @Autowired
    MasterService masterService;

    @PostMapping("/data/internal/account/add")
    public Object addAccount(@RequestBody AccountDTO request) {
        return masterService.saveAccount(request, DTOConverter::convertAccount);
    }

    @PostMapping("/data/internal/account/addList")
    public Object addAccount(@RequestBody List<AccountDTO> request) {
        return masterService.saveMultipleAccount(request);
    }

    @PostMapping("/data/internal/account/update")
    public Object editAccount(@RequestBody AccountDTO accountDTO) throws Exception {
        return masterService.updateAccount(accountDTO, DTOConverter::convertAccount);
    }

    @PostMapping("/data/internal/account/delete")
    public boolean removeAccount(@RequestBody AccountDTO accountDTO, @RequestParam(defaultValue = "false") boolean hardDelete) throws Exception {
        return hardDelete ? masterService.hardDelete(accountDTO) : masterService.softDelete(accountDTO);
    }


    @GetMapping("/data/internal/account/email")
    public Object findEmail(@RequestParam String email) throws Exception {
        return masterService.findUserByEmail(email, DTOConverter::convertAccount);
    }

    @GetMapping("/data/internal/account/list/page")
    public Object getPagelist(@RequestParam Map<Object, Object> params) {
        Object response = masterService.getPageableList(PageAccountRequest.builder()
                .page(BaseUtil.getInt(params.get("page")))
                .size(BaseUtil.getInt(params.get("size")))
                .task(BaseUtil.getStr(params.get("task")))
                .build());
        return response;
    }

}
