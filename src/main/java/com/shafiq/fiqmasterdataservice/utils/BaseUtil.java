package com.shafiq.fiqmasterdataservice.utils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BaseUtil {
    public static String getStr(Object obj) {
        if (obj != null) {
            return obj.toString().trim();
        } else {
            return "";
        }
    }

    public static Integer getInt(Object obj) {
        if (obj != null) {
            return Integer.parseInt(obj.toString().trim());
        } else {
            return 0;
        }
    }

    public static double getDouble(Object obj) {
        if (obj != null) {
            return Double.parseDouble(obj.toString().trim());
        } else {
            return 0.0;
        }
    }

    public static float getFloat(Object obj) {
        if (obj != null) {
            return Float.parseFloat(obj.toString().trim());
        } else {
            return 0;
        }
    }

    public static String getStrUpper(Object obj) {
        if (obj != null) {
            return obj.toString().trim().toUpperCase();
        } else {
            return "";
        }
    }

    public static String getStrLower(Object obj) {
        if (obj != null) {
            return obj.toString().trim().toLowerCase();
        } else {
            return "";
        }
    }

    public static String genTwoAutoId(int currentNo) {
        String genNo = "" + currentNo;
        return "00".substring(genNo.length()) + genNo;
    }

    public static String genThreeAutoId(int currentNo) {
        String genNo = "" + currentNo;
        return "000".substring(genNo.length()) + genNo;
    }

    public static String genFourAutoId(int currentNo) {
        String genNo = "" + currentNo;
        return "0000".substring(genNo.length()) + genNo;
    }

    public static String generatePassword() {
        Random random = new Random();
        String tempPass = "";
        String alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdeghijklmnopqrstuvwxyz";
        int character = alphabet.length();

        for (int i = 0; i < 8; i++) {
            tempPass = tempPass + alphabet.charAt(random.nextInt(character));
        }

        return tempPass;
    }

    public static String generateUniqueID() {
        Calendar calendar = Calendar.getInstance();
        int year = 0;
        year = calendar.get(Calendar.YEAR);
        int month = 0;
        month = calendar.get(Calendar.MONTH);
        int day = 0;
        day = calendar.get(Calendar.DATE);

        String prefix = getPnemonicYear(year) + getPnemonicMonth(month + 1) + getCounterText2Digit(day);

        String alphanumeric = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int N = alphanumeric.length();
        Random r = new Random();

        String uniqueID = "";
        for (int i = 0; i < 8; i++) {
            uniqueID = uniqueID + alphanumeric.charAt(r.nextInt(N));
        }

        uniqueID = prefix + uniqueID;

        return uniqueID;
    }

    public static String generateUniqueAppNumber() {
        String alphanumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int N = alphanumeric.length();
        Random r = new Random();

        String uniqueID = "";
        for (int i = 0; i < 5; i++) {
            uniqueID = uniqueID + alphanumeric.charAt(r.nextInt(N));
        }

        return uniqueID;
    }

    public static String generateRandomNumber() {
        String number = "1234567890";
        int N = number.length();
        Random r = new Random();

        String uniqueID = "";
        for (int i = 0; i < 3; i++) {
            uniqueID = uniqueID + number.charAt(r.nextInt(N));
        }

        return uniqueID;
    }

    public static String properCase(String value) {
        Pattern pattern = Pattern.compile("(^|\\W)([a-z])");
        Matcher matcher = pattern.matcher(value.toLowerCase());
        StringBuffer sb = new StringBuffer(value.length());
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1) + matcher.group(2).toUpperCase());
        }
        matcher.appendTail(sb);

        return sb.toString();
    }

    public static String getLastAlphabet(String word, int count) {
        String[] txtList;
        String appWord = "";

        if (word != null) {
            txtList = BaseUtil.split(word, 1);
            int deduct = txtList.length - count;
            if (txtList != null) {
                for (int i = deduct; i < txtList.length; i++) {
                    appWord = appWord + txtList[i];
                }
            }
        }

        return appWord;
    }

    public static String[] split(String mainStr, int size) {
        Vector vector = new Vector();
        int j;
        String s;

        if (mainStr == null) {
            mainStr = "";
        }

        while (mainStr.equals("") == false) {
            if (mainStr.length() > size) {
                s = mainStr.substring(0, size);
                mainStr = mainStr.substring(size);
            } else {
                s = mainStr;
                mainStr = "";
            }

            vector.addElement(s);
        }

        try {
            if (vector == null || vector.size() == 0) {
                return null;
            }
            String[] valuesArray = new String[vector.size()];
            vector.copyInto(valuesArray);

            return valuesArray;
        } catch (Exception ex) {
            return null;
        }
    }

    public static String WebString(String s) {
        String y;

        y = s;
        y = replace(y, "/", "%2F");
        y = replace(y, "'", "%27");
        y = replace(y, "\"", "%22");
        y = replace(y, "=", "%3D");
        y = replace(y, " ", "%20");
        y = replace(y, "%", "%25");
        y = replace(y, "&", "AND");
        y = replace(y, "<P>", "");
        y = replace(y, "</P>", "");
        y = replace(y, "<p>", "");
        y = replace(y, "</p>", "");

        return y;
    }

    public static String replace(String s, String oldstr, String newstr) {
        boolean finish = true;
        String y = "";
        int i;

        while (finish) {
            i = s.indexOf(oldstr);
            if (i == -1) {
                y = y + s;
                break;
            }

            y = y + s.substring(0, i) + newstr;
            s = s.substring(i + oldstr.length());
        }

        return y;
    }

    public static String format(double d, boolean comma, int decimalpoint) {
        DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
        if (comma == false) {
            df.setGroupingSize(0);
        } else {
            df.setGroupingSize(3);
        }

        df.setMaximumFractionDigits(decimalpoint);
        df.setMinimumFractionDigits(decimalpoint);

        return df.format(d);
    }

    public static String getCounterText7Digit(int count) {
        String textCounter = "";

        if (count < 10) {
            textCounter = "000000" + count;
        } else if (count < 100 && count >= 10) {
            textCounter = "00000" + count;
        } else if (count < 1000 && count >= 100) {
            textCounter = "0000" + count;
        } else if (count < 10000 && count >= 1000) {
            textCounter = "000" + count;
        } else if (count < 100000 && count >= 10000) {
            textCounter = "00" + count;
        } else if (count < 1000000 && count >= 100000) {
            textCounter = "0" + count;
        } else if (count < 10000000 && count >= 1000000) {
            textCounter = Integer.toString(count);
        }

        return textCounter;
    }

    public static String getPnemonicYear(int year) {
        String y;

        switch (year) {
            //the choices go here - print the details
            case 2011:
                y = "A";
                break;
            case 2012:
                y = "B";
                break;
            case 2013:
                y = "C";
                break;
            case 2014:
                y = "D";
                break;
            case 2015: //exit
                y = "E";
                break;
            case 2016: //exit
                y = "F";
                break;
            case 2017: //exit
                y = "G";
                break;
            case 2018: //exit
                y = "H";
                break;
            case 2019: //exit
                y = "I";
                break;
            case 2020: //exit
                y = "J";
                break;
            case 2021: //exit
                y = "K";
                break;
            case 2022: //exit
                y = "L";
                break;
            case 2023: //exit
                y = "M";
                break;
            case 2024: //exit
                y = "N";
                break;
            case 2025: //exit
                y = "O";
                break;
            case 2026: //exit
                y = "P";
                break;
            case 2027: //exit
                y = "Q";
                break;
            case 2028: //exit
                y = "R";
                break;
            case 2029: //exit
                y = "S";
                break;
            case 2030: //exit
                y = "T";
                break;
            case 2031: //exit
                y = "U";
                break;
            case 2032: //exit
                y = "V";
                break;
            case 2033: //exit
                y = "W";
                break;
            case 2034: //exit
                y = "X";
                break;
            case 2035: //exit
                y = "Y";
                break;
            case 2036: //exit
                y = "Z";
                break;

            default:
                y = "A";
        }

        return y;
    }

    public static String getPnemonicMonth(int month) {
        String m;

        switch (month) {
            //the choices go here - print the details
            case 1:
                m = "1";
                break;
            case 2:
                m = "2";
                break;
            case 3:
                m = "3";
                break;
            case 4:
                m = "4";
                break;
            case 5: //exit
                m = "5";
                break;
            case 6: //exit
                m = "6";
                break;
            case 7: //exit
                m = "7";
                break;
            case 8: //exit
                m = "8";
                break;
            case 9: //exit
                m = "9";
                break;
            case 10: //exit
                m = "0";
                break;
            case 11: //exit
                m = "A";
                break;
            case 12: //exit
                m = "B";
                break;

            default:
                m = "X";//wrong
        }

        return m;
    }

    public static String getCounterText2Digit(int count) {
        String textCounter = "";

        if (count < 10) {
            textCounter = "0" + count;
        } else if (count < 100 && count >= 10) {
            textCounter = Integer.toString(count);
        }

        return textCounter;
    }

    public static String getCounterText3Digit(int count) {
        String textCounter = "";
        if (count < 10) {
            textCounter = "00" + count;
        } else if (count < 100 && count >= 10) {
            textCounter = "0" + count;
        } else if (count < 1000 && count >= 100) {
            textCounter = Integer.toString(count);
        }

        return textCounter;
    }

    public static String getCounterText4Digit(int count) {
        String textCounter = "";
        if (count < 10) {
            textCounter = "000" + count;
        } else if (count < 100 && count >= 10) {
            textCounter = "00" + count;
        } else if (count < 1000 && count >= 100) {
            textCounter = "0" + count;
        } else if (count < 10000 && count >= 1000) {
            textCounter = Integer.toString(count);
        }

        return textCounter;
    }

    public static String getAppPolicyNumber() {
        String policyNumber = "";
        Calendar calendar = Calendar.getInstance();
        int year = 0;
        year = calendar.get(Calendar.YEAR);
        int month = 0;
        month = calendar.get(Calendar.MONTH);
        int day = 0;
        day = calendar.get(Calendar.DATE);

        String prefix = "O" + getPnemonicYear(year) + getPnemonicMonth(month + 1) + getCounterText2Digit(day);

        String uniqueAppNumber = generateUniqueAppNumber();

        String randomNumber = generateRandomNumber();

        policyNumber = "INS/" + prefix + "/" + uniqueAppNumber + randomNumber;

        return policyNumber;
    }


    public static String getAppNumber() {
        String appNumber = "";
        Calendar calendar = Calendar.getInstance();
        int year = 0;
        year = calendar.get(Calendar.YEAR);
        int month = 0;
        month = calendar.get(Calendar.MONTH);
        int day = 0;
        day = calendar.get(Calendar.DATE);

        String prefix = "O" + getPnemonicYear(year) + getPnemonicMonth(month + 1) + getCounterText2Digit(day);

        String uniqueAppNumber = generateUniqueAppNumber();

        String randomNumber = generateRandomNumber();

        appNumber = "WM/" + prefix + "/" + uniqueAppNumber + randomNumber;

        return appNumber;
    }

    public static String getAlpha(int num) {
        String alpha = "";
        int count = 0;
        for (char letter = 'A'; letter <= 'Z'; letter = (char) (letter + '\001')) {
            if (count == num) {
                alpha = Character.toString(letter);
                break;
            }
            count++;
        }

        return alpha;
    }

    public static String format(Date d, String s) {
        SimpleDateFormat formatter = new SimpleDateFormat(s);
        if (d == null) {
            return "";
        } else {
            return formatter.format(d);
        }
    }

    public static String nosymbol(String s) {
        String y;

        y = s;
        y = replace(y, "&", "");
        y = replace(y, "'", "");
        y = replace(y, "'", "");
        y = replace(y, "#", "");
        y = replace(y, "(", "");
        y = replace(y, ")", "");
        y = replace(y, "\"", "");
        y = replace(y, "/", "");
        y = replace(y, "-", "");
        y = replace(y, ":", "");
        y = replace(y, "<", "");
        y = replace(y, ">", "");
        y = replace(y, "'", "");
        y = replace(y, ".", "");
        y = replace(y, " ", "");
        y = replace(y, "*", "");
        y = replace(y, "!", "");
        y = replace(y, "@", "");
        y = replace(y, "$", "");
        y = replace(y, "%", "");
        y = replace(y, "_", "");
        y = replace(y, "=", "");
        y = replace(y, "+", "");
        y = replace(y, "?", "");

        return y;

    }

    public static String getCustomerAgingType(Date fromDt, Date toDt) {

        String agingType = "";
        SimpleDateFormat year = new SimpleDateFormat("yyyy");
        SimpleDateFormat month = new SimpleDateFormat("M");
        SimpleDateFormat day = new SimpleDateFormat("d");

        int fYear = getInt(year.format(fromDt));
        int fMonth = getInt(month.format(fromDt));
        int fDay = getInt(day.format(fromDt));

        int tYear = getInt(year.format(toDt));
        int tMonth = getInt(month.format(toDt));
        int tDay = getInt(day.format(toDt));

        LocalDate dtFrom = LocalDate.of(fYear, fMonth, fDay);
        LocalDate dtTo = LocalDate.of(tYear, tMonth, tDay);

        Period age = Period.between(dtFrom, dtTo);
        System.out.printf("%d years, %d months and %d days.%n", age.getYears(), age.getMonths(), age.getDays());

        if (age.getYears() >= 18) {
            agingType = "ADT";
        } else if (age.getYears() >= 1 && age.getYears() <= 17) {
            agingType = "CHD";
        } else if (age.getYears() < 1) {
            agingType = "INF";
        }

        return agingType;
    }

    public static String generateCode() {
        Random random = new Random();
        String tempPass = "";
        String alphabet = "abcdeghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int character = alphabet.length();

        for (int i = 0; i < 6; i++) {
            tempPass = tempPass + alphabet.charAt(random.nextInt(character));
        }

        return tempPass;
    }

    public static String convertDate(String date) {
        System.out.println("date : " + date);
        SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat to = new SimpleDateFormat("dd MMM yyyy");
        String strDateTime = null;
        try {
            strDateTime = to.format(from.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println("strDateTime : " + strDateTime);
        return strDateTime;
    }

    public static String convertDate2(Date date) {
        System.out.println("date : " + date);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = "";
        if (date == null) {
            strDate = "";
        } else {
            strDate = formatter.format(date);
        }

        return strDate;
    }

    public static String capitalizeWord(String str) {
        String capitalizeWord = "";
        try {

            if (str != null && str.equalsIgnoreCase("") == false) {
                String[] words = str.split("\\s");
                for (String w : words) {
                    String first = w.substring(0, 1);
                    String afterfirst = w.substring(1);
                    capitalizeWord += first.toUpperCase() + afterfirst.toLowerCase() + " ";
                }
            }

        } catch (Exception e) {
            //System.out.println("capitalizeWord :: " + e);
        }

        return capitalizeWord.trim();
    }

    public static class Convention {
        public static String Year() {
            int baseYear = 2011;

            Calendar c = Calendar.getInstance();
            int iYear = c.get(1);
            int diffYear = iYear - baseYear;

            String sYear = Integer.toString(baseYear);
            sYear = getAlpha(diffYear);

            return sYear;
        }

        public static String Month() {
            Calendar c = Calendar.getInstance();
            int iMonth = c.get(2);

            String sMonth = getAlpha(iMonth);

            return sMonth;
        }

        public static String Date() {
            Calendar c = Calendar.getInstance();
            int iDate = c.get(5);

            String sDate = "";
            if (iDate < 10) {
                sDate = Integer.toString(iDate);
            } else {
                sDate = getAlpha(iDate - 9);
            }

            return sDate;
        }

        public static String Hour() {
            Calendar c = Calendar.getInstance();
            int iHour = c.get(11);

            String sHour = "";
            sHour = getAlpha(iHour);

            return sHour;
        }

        public static String Time() {
            return format(new Date(), "mmss");
        }
    }
}

