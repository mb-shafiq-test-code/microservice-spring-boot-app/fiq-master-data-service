package com.shafiq.fiqmasterdataservice.services;

import com.shafiq.fiqmasterdataservice.controller.dto.AccountDTO;
import com.shafiq.fiqmasterdataservice.controller.dto.PageAccountRequest;
import com.shafiq.fiqmasterdataservice.entities.Account;
import com.shafiq.fiqmasterdataservice.repositories.AccountRepository;
import com.shafiq.fiqmasterdataservice.utils.BaseUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import java.util.*;
import java.util.function.Function;

@Service
@Slf4j
public class MasterService {
    @Autowired
    AccountRepository accountRepository;

    @Transactional
    public <T> T saveAccount(AccountDTO p, Function<Account, T> f) {

        try {
            Account account = new Account();
            account.setFullName(Optional.ofNullable(p.getFullName()).orElse(null));
            account.setEmail(Optional.ofNullable(p.getEmail()).orElse(null));
            account.setMobileNo(Optional.ofNullable(p.getMobileNo()).orElse(null));
            account.setBirthDate(Optional.ofNullable(p.getBirthDate()).orElse(null));
            account.setTask(Optional.ofNullable(p.getTask()).orElse(null));
            return f.apply(accountRepository.save(account));

        } catch (Exception ex) {
            log.error("Email Already Exist" + ex.getMessage());
            log.error(ex.getMessage());
            return null;
        }

    }

    @Transactional
    public Object saveMultipleAccount(List<AccountDTO> listUserAccount) {

        try {
            List<Account> listAccout = new ArrayList<>();
            for(AccountDTO p : listUserAccount){
                Account account = new Account();
                account.setFullName(Optional.ofNullable(p.getFullName()).orElse(null));
                account.setEmail(Optional.ofNullable(p.getEmail()).orElse(null));
                account.setMobileNo(Optional.ofNullable(p.getMobileNo()).orElse(null));
                account.setBirthDate(Optional.ofNullable(p.getBirthDate()).orElse(null));
                account.setTask(Optional.ofNullable(p.getTask()).orElse(null));

                listAccout.add(account);
            }

            return accountRepository.saveAll(listAccout);

        } catch (Exception ex) {
            log.error("Email Already Exist" + ex.getMessage());
            log.error(ex.getMessage());
            return null;
        }

    }

    @Transactional
    public <T> T updateAccount(AccountDTO p, Function<Account, T> f) {

        Account account = accountRepository.findByEmail(p.getEmail())
                .filter(a -> !a.getDeleted())
                .map(a -> {
                    a.setFullName(Optional.ofNullable(p.getFullName()).orElse(a.getFullName()));
                    a.setMobileNo(Optional.ofNullable(p.getMobileNo()).orElse(a.getMobileNo()));
                    a.setBirthDate(Optional.ofNullable(p.getBirthDate()).orElse(a.getBirthDate()));
                    a.setTask(Optional.ofNullable(p.getTask()).orElse(a.getTask()));
                    return a;
                }).orElse(null);
        if (Objects.isNull(account)) {
            return null;
        }
        return f.apply(accountRepository.save(account));
    }

    @Transactional
    public boolean hardDelete(AccountDTO p) {

        Account account = accountRepository.findByEmail(p.getEmail())
                .map(a -> {
                    a.setId(Optional.ofNullable(p.getId()).orElse(a.getId()));
                    a.setEmail(Optional.ofNullable(p.getEmail()).orElse(a.getEmail()));
                    return a;
                }).orElse(null);
        if (Objects.isNull(account)) {
            return false;
        }
        accountRepository.delete(account);
        return true;
    }

    @Transactional
    public boolean softDelete(AccountDTO p) {

        Account account = accountRepository.findByEmail(p.getEmail())
                .filter(a -> !a.getDeleted())
                .map(a -> {
                    a.setDeleted(true);
                    return a;
                }).orElse(null);
        if (Objects.isNull(account)) {
            return false;
        }
        accountRepository.save(account);
        return true;
    }

    public <T> T findUserByEmail(String email, Function<Account, T> f) {

        Account account = accountRepository.findByEmail(email)
                .orElse(null);
        return f.apply(account);
    }

    public Map<Object, Object> getPageableList(PageAccountRequest request) {
        Sort defaultSort = Sort.by("lastUpdateDate").descending();
        Pageable defaultPage = PageRequest.of(request.getPage(), request.getSize(), defaultSort);

        Page<Account> pageList = accountRepository.findAll(createSpecification(request), defaultPage);

        Integer totalPage = BaseUtil.getInt(pageList.getTotalPages());
        Integer totalElement = BaseUtil.getInt(pageList.getTotalElements());

        List<Account> listObj = new ArrayList<>();
        if (pageList.hasContent()) {
            listObj = pageList.getContent();
        }

        Map<Object, Object> map = new HashMap();
        map.put("totalPage", totalPage);
        map.put("totalElement", totalElement);
        map.put("lists", Collections.singletonList(listObj));

        return map;
    }

    private Specification<Account> createSpecification(PageAccountRequest request) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> list = new ArrayList<>();
            if (!Objects.isNull(request.getTask()) && StringUtils.isNotEmpty(request.getTask())) {
                list.add(criteriaBuilder.equal(root.get("task"), request.getTask()));
            }
            return criteriaBuilder.and(list.toArray(new Predicate[0]));
        };
    }

}
