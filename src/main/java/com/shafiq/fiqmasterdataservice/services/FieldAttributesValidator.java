package com.shafiq.fiqmasterdataservice.services;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.shafiq.fiqmasterdataservice.utils.BaseUtil;
import com.shafiq.fiqmasterdataservice.utils.DateUtil;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
public final class FieldAttributesValidator {
    public static final String VALIDATION_FAILED_CODE = "101";
    public static final String VALIDATION_FAILED_MESSAGE = "Failed due to ";
    public static final String ISO_DATE_FORMAT = "ISO_DATE_FORMAT";
    public static final String UTC_DATE_FORMAT = "UTC_DATE_FORMAT";
    private static final String ERROR_MANDATORY = "%s cannot be null";
    private static final String ERROR_EMAIL_FORMAT = "%s entered is wrong email format";
    private static final String ERROR_MIN_LENGTH = "%s minimum length is %s";
    private static final String ERROR_MAX_LENGTH = "%s maximum length is %s";
    private static final String ERROR_SIZE = "%s length must be %s";
    private static final String ERROR_MIN_VALUE = "%s must be greater than or equal to %s";
    private static final String ERROR_MAX_VALUE = "%s must be less than or equal to %s";
    private static final String ERROR_ISO_DATE = "%s date format must be ISO 8601";
    private static final String ERROR_UTC_DATE = "%s date format must be UTC Format Date Time";
    private static final String ERROR_DATE_FORMAT = "%s date format must be %s";
    private static final String ERROR_INVALID_DATE = "%s invalid date";
    private static final String ERROR_VALUE = "%s value must be %s";
    private static final String ERROR_OPTIONAL_MANDATORY = "%s Either one cannot be null";

    /*public static ResponseInfo validateRequest(Object requestObject) {
        String validateRequest = FieldAttributesValidator.validate(requestObject);
        if (validateRequest != null) {
            log.info("validateRequest ? : " + validateRequest);
            return ResponseInfo.builder()
                    .responseStatus("F")
                    .responseCode(VALIDATION_FAILED_CODE)
                    .responseMessage(VALIDATION_FAILED_MESSAGE + validateRequest)
                    .build();
        }
        return null;
    }*/

    public static String validate(Object object) {
        Field[] fields = object.getClass().getDeclaredFields();
        List<OptionalObject> listOptionalMandatoryGroupObject = new ArrayList();
        for (Field field : fields) {
            field.setAccessible(true);
            FieldAttributes attribute = field.getAnnotation(FieldAttributes.class);
            String fieldName = getFieldName(field);
            if (attribute != null) {
                Object value = null;
                try {
                    value = field.get(object);
                } catch (IllegalAccessException e) {
                }

                if (!attribute.nullable() && (value == null || StringUtils.isEmpty(value.toString()))) {
                    return String.format(ERROR_MANDATORY, fieldName);
                }

                if (value == null)
                    continue;

                if (attribute.isEmail() && (value != null || !StringUtils.isEmpty(value.toString()))) {

                    String regex = "^(.+)@(.+)$";
                    Pattern pattern = Pattern.compile(regex);
                    Matcher matcher = pattern.matcher(value.toString());
                    if(!matcher.matches()){
                        return String.format(ERROR_EMAIL_FORMAT, fieldName);
                    }
                }

                if (isClassType(field)) {
                    Object childObject = value;
                    String check = validate(childObject);
                    if (check != null) {
                        return check;
                    }
                }

                if (attribute.min() > value.toString()
                        .length())
                    return String.format(ERROR_MIN_LENGTH, fieldName, attribute.min());

                if (attribute.max() < value.toString()
                        .length())
                    return String.format(ERROR_MAX_LENGTH, fieldName, attribute.max());

                if (attribute.size() > 0 && attribute.size() != value.toString()
                        .length())
                    return String.format(ERROR_SIZE, fieldName, attribute.size());

                if (isNumericType(field) && attribute.minValue() > Double.valueOf(value.toString()))
                    return String.format(ERROR_MIN_VALUE, fieldName, attribute.minValue());

                if (isNumericType(field) && attribute.maxValue() < Double.valueOf(value.toString()))
                    return String.format(ERROR_MAX_VALUE, fieldName, attribute.maxValue());

                if (attribute.dateFormat().length > 0) {
                    try {
                        if (attribute.dateFormat()[0].equals(ISO_DATE_FORMAT)) {
                            if (!DateUtil.isISO8601(value.toString()))
                                return String.format(ERROR_ISO_DATE, fieldName);
                        } else if (attribute.dateFormat()[0].equals(UTC_DATE_FORMAT)) {
                            if (!DateUtil.isISO8601(value.toString()))
                                return String.format(ERROR_UTC_DATE, fieldName);
                        } else {
                            if (new SimpleDateFormat(attribute.dateFormat()[0]).parse(value.toString()) == null)
                                return String.format(ERROR_DATE_FORMAT, fieldName, attribute.dateFormat()[0]);

                        }
                    } catch (java.text.ParseException ex) {
                        return String.format(ERROR_INVALID_DATE, fieldName);
                    }
                }

                if (attribute.allowableValues().length > 0) {
                    if (!Arrays.stream(attribute.allowableValues()).anyMatch(value.toString()::equals)) {
                        return String.format(ERROR_VALUE, fieldName, String.join(",", attribute.allowableValues()));
                    }
                }

                if (!BaseUtil.getStr(attribute.optionalMandatoryGroup()).equals("")) {

                    String groupOptionalGroupName = BaseUtil.getStr(attribute.optionalMandatoryGroup());
                    listOptionalMandatoryGroupObject.add(OptionalObject.builder()
                            .groupField(fieldName)
                            .groupValue(value.toString())
                            .groupName(groupOptionalGroupName)
                            .build());
                }

            }
        }
        //CHECK OPTIONAL MANDATORY BY GROUP
        Map<String, List<OptionalObject>> grouplistGrouped = listOptionalMandatoryGroupObject.stream().collect(Collectors.groupingBy(w -> w.groupName));

        for (String key : grouplistGrouped.keySet()) {
            String group = key;
            List<OptionalObject> listObject = grouplistGrouped.get(key);
            int hasValueCount = 0;
            StringJoiner fieldNameSet = new StringJoiner(",");
            for (OptionalObject o : listObject) {
                fieldNameSet.add(o.groupField);
                if (!BaseUtil.getStr(o.groupValue).equals("")) {
                    hasValueCount++;
                }
            }

            if (hasValueCount == 0) {
                return String.format(ERROR_OPTIONAL_MANDATORY, fieldNameSet);
            }
        }

        return null;
    }

    private static boolean isNumericType(Field f) {
        return f.getType().equals(Integer.class)
                || f.getType().equals(int.class)
                || f.getType().equals(Double.class)
                || f.getType().equals(double.class)
                || f.getType().equals(Float.class)
                || f.getType().equals(float.class);
    }

    private static boolean isClassType(Field f) {
        return !isNumericType(f)
                && !f.getType().equals(String.class)
                && !f.getType().equals(Boolean.class)
                && !f.getType().equals(Date.class);
    }

    private static String getFieldName(Field field) {
        JsonProperty jsonProperty = field.getAnnotation(JsonProperty.class);
        if (jsonProperty != null) {
            return jsonProperty.value();
        } else {
            return field.getName();
        }
    }

    @Builder
    @Data
    public static class OptionalObject {
        private String groupField;
        private String groupValue;
        private String groupName;

    }
}
