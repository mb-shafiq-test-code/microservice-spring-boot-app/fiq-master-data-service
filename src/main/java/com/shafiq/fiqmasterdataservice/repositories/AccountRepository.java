package com.shafiq.fiqmasterdataservice.repositories;

import com.shafiq.fiqmasterdataservice.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account,Object>,JpaSpecificationExecutor<Account> {

    Optional<Account> findByEmail(String email);

}
